import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'comment',
        loadChildren: () => import('./jawnapp/comment/comment.module').then(m => m.JawnappCommentModule),
      },
      {
        path: 'tag',
        loadChildren: () => import('./jawnapp/tag/tag.module').then(m => m.JawnappTagModule),
      },
      {
        path: 'product',
        loadChildren: () => import('./jawnapp/product/product.module').then(m => m.JawnappProductModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class JawngatewayEntityModule {}
