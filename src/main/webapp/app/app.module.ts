import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { JawngatewaySharedModule } from 'app/shared/shared.module';
import { JawngatewayCoreModule } from 'app/core/core.module';
import { JawngatewayAppRoutingModule } from './app-routing.module';
import { JawngatewayHomeModule } from './home/home.module';
import { JawngatewayEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    JawngatewaySharedModule,
    JawngatewayCoreModule,
    JawngatewayHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    JawngatewayEntityModule,
    JawngatewayAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class JawngatewayAppModule {}
