import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IProduct } from 'app/shared/model/jawnapp/product.model';
import { ITag } from 'app/shared/model/jawnapp/tag.model';

export interface IComment {
  id?: number;
  content?: any;
  createDate?: Moment;
  user?: IUser;
  product?: IProduct;
  parent?: IComment;
  tags?: ITag[];
}

export class Comment implements IComment {
  constructor(
    public id?: number,
    public content?: any,
    public createDate?: Moment,
    public user?: IUser,
    public product?: IProduct,
    public parent?: IComment,
    public tags?: ITag[]
  ) {}
}
