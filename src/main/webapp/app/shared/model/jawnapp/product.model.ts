import { Moment } from 'moment';
import { ITag } from 'app/shared/model/jawnapp/tag.model';
import { Department } from 'app/shared/model/enumerations/department.model';

export interface IProduct {
  id?: number;
  title?: string;
  name?: string;
  handle?: string;
  description?: string;
  review?: any;
  price?: number;
  createDate?: Moment;
  imageContentType?: string;
  image?: any;
  link?: string;
  department?: Department;
  tags?: ITag[];
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public title?: string,
    public name?: string,
    public handle?: string,
    public description?: string,
    public review?: any,
    public price?: number,
    public createDate?: Moment,
    public imageContentType?: string,
    public image?: any,
    public link?: string,
    public department?: Department,
    public tags?: ITag[]
  ) {}
}
