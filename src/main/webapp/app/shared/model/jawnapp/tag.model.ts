import { IComment } from 'app/shared/model/jawnapp/comment.model';
import { IProduct } from 'app/shared/model/jawnapp/product.model';

export interface ITag {
  id?: number;
  name?: string;
  comments?: IComment[];
  products?: IProduct[];
}

export class Tag implements ITag {
  constructor(public id?: number, public name?: string, public comments?: IComment[], public products?: IProduct[]) {}
}
