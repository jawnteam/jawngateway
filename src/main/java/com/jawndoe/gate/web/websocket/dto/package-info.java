/**
 * Data Access Objects used by WebSocket services.
 */
package com.jawndoe.gate.web.websocket.dto;
