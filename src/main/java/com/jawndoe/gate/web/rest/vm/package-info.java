/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jawndoe.gate.web.rest.vm;
